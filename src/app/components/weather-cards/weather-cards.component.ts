import { Component, OnInit } from '@angular/core';
import { WeatherService } from 'src/app/services/weather.service';

@Component({
  selector: 'app-weather-cards',
  templateUrl: './weather-cards.component.html',
  styleUrls: ['./weather-cards.component.scss']
})
export class WeatherCardsComponent implements OnInit {

  cities: any;

  constructor(private ws: WeatherService) { }

  ngOnInit(): void {
    this.getCities();
  }

  getCities(): void {
    this.ws
      .getEuropeanCities()
      .subscribe((data: any) => {
        this.cities = data.list;
      });
  }

}
