import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { WeatherService } from 'src/app/services/weather.service';

import { WeatherCardsComponent } from './weather-cards.component';

describe('WeatherCardsComponent', () => {
  let component: WeatherCardsComponent;
  let fixture: ComponentFixture<WeatherCardsComponent>;
  let ws: WeatherService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WeatherCardsComponent],
      imports: [HttpClientTestingModule],
      providers: [
        WeatherService
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherCardsComponent);
    component = fixture.componentInstance;
    ws = TestBed.get(WeatherService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('getCities should load the cities', () => {
    spyOn(ws, 'getEuropeanCities').and.returnValue(of([]));
    fixture.detectChanges();
    fixture.componentInstance.getCities();
    expect(ws.getEuropeanCities).toHaveBeenCalledWith();
  });
});
