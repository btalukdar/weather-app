import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { WeatherService } from 'src/app/services/weather.service';

import { WeatherDetailComponent } from './weather-detail.component';

describe('WeatherDetailComponent', () => {
  let component: WeatherDetailComponent;
  let fixture: ComponentFixture<WeatherDetailComponent>;
  let ws: WeatherService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [WeatherDetailComponent],
      imports: [RouterTestingModule, HttpClientTestingModule],
      providers: [
        WeatherService,
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ id: '123' })
          }
        }
      ]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherDetailComponent);
    component = fixture.componentInstance;
    ws = TestBed.get(WeatherService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('getForecastByCity should load the forecast details', () => {
    spyOn(ws, 'getForecastByCity').and.returnValue(of([]));
    fixture.detectChanges();
    fixture.componentInstance.ngOnInit();
    expect(ws.getForecastByCity).toHaveBeenCalledWith('123');
  });
});
