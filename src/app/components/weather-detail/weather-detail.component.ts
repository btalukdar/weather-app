import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { WeatherService } from 'src/app/services/weather.service';

@Component({
  selector: 'app-weather-detail',
  templateUrl: './weather-detail.component.html',
  styleUrls: ['./weather-detail.component.scss']
})
export class WeatherDetailComponent implements OnInit {

  weatherDetail: any;
  weatherList: any;

  constructor(
    private actRoute: ActivatedRoute,
    private router: Router,
    private ws: WeatherService,
  ) { }

  ngOnInit(): void {
    this.actRoute.params.subscribe(params => {
      if (params['id']) {
        this.ws
          .getForecastByCity(params['id'])
          .subscribe((data: any) => {
            this.weatherDetail = data;
            if (data.list && data.list.length > 0) {
              this.formatWeather(data.list);
            }
          });
      } else {
        this.router.navigate(['/']);
      }
    });
  }

  formatWeather(list: any) {
    this.weatherList = list.filter((item: any) => {
      const time = new Date(item.dt_txt).getHours();
      return time === 9;
    });
  }

}
