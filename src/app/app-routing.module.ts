import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WeatherCardsComponent } from './components/weather-cards/weather-cards.component';
import { WeatherDetailComponent } from './components/weather-detail/weather-detail.component';

const routes: Routes = [
  { path: 'weather', component: WeatherCardsComponent, pathMatch: 'full' },
  { path: 'weather/:id', component: WeatherDetailComponent, pathMatch: 'full' },
  {
    path: '',
    redirectTo: '/weather',
    pathMatch: 'full'
  },
  { path: '**', redirectTo: 'weather' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
