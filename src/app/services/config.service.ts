import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  BASE_URL = 'http://api.openweathermap.org/data/2.5/';
  APP_ID = 'b67f055a8e6dbeaca85ebef2b352b3e1';

  constructor() { }

  getBaseUrl(): string {
    return this.BASE_URL;
  }

  getAppId(): string {
    return this.APP_ID;
  }
}
