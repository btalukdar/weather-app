import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { from, Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ConfigService } from './config.service';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  API_URL = '';
  APP_ID = '';

  constructor(
    private httpClient: HttpClient,
    private cs: ConfigService,
  ) {
    this.API_URL = this.cs.getBaseUrl();
    this.APP_ID = this.cs.getAppId();
  }

  getHeaders() {
    let headers: HttpHeaders = new HttpHeaders();
    headers = headers.append('Accept', 'application/json');
    return headers;
  }

  getEuropeanCities(): Observable<any> {
    let headers = this.getHeaders();
    const url = this.API_URL + 'find?' + 'units=metric&cnt=5&lat=51.5085&lon=-0.1257&cnt=50&appid=' + this.APP_ID;
    return this.httpClient.get(
      url,
      { headers: headers }
    ).pipe(
      map((data: any) => {
        return data;
      }), catchError(error => {
        return throwError('Data not found!');
      })
    )
  }

  getForecastByCity(cityId: string): Observable<any> {
    let headers = this.getHeaders();
    const url = this.API_URL + 'forecast?' + 'id=' + cityId + '&cnt=35&units=metric&appid=' + this.APP_ID;
    return this.httpClient.get(
      url,
      { headers: headers }
    ).pipe(
      map((data: any) => {
        return data;
      }), catchError(error => {
        return throwError('Data not found!');
      })
    )
  }
}
