# WeatherApp

Display Real Time Weather report of 5 European Cities
## Development server

Run `npm install` to setup all dependencies

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
